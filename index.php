<?php get_header(); ?>
<!-- index -->
	<section id="features" class="row">
		
		<?php 
		// the query
		
		$heroQuery = array(
			'meta_key' => 'portadaHome',
			'meta_value' => 1
		);
		
		$the_query = new WP_Query( $heroQuery ); ?>

		<?php if ( $the_query->have_posts() ) : ?>
		  <!-- the loop -->
		  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		
		<article class="col_1c">
			<?php if ( has_post_thumbnail()) : ?>
		   <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
		   <?php 
			$heroAttr = array (
							'class'=> "col_3c"
						);
			the_post_thumbnail('large',$heroAttr); ?>
		   </a>
		 	<?php endif; ?>
			


			<div class="col_3">
	 			<h2> <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
				<?php the_title(); ?>
				</a>
				</h2>
				<p><?php the_excerpt(); ?></p>
				<p><a href="<?php the_permalink(); ?>">Más información »</a></p>
			</div>
		</article>
		
		  <?php endwhile; ?>
		  <!-- end of the loop -->

		  <!-- pagination here -->

		  <?php wp_reset_postdata(); ?>

		<?php endif; ?>
		
			
	</section>
	
	
	<section class="row">
		<div id="featuresNav" class="centered">
				<ul class="dots"></ul>
		</div>
	</section>
	
	<div class="banner">
		<section class="row">
			<div class="col_1">
				<h3>Temas</h3>				
			</div>
			
		</section>
		
		
		

	</div>
	
	<section class="row">
		
		<?php 
		// the query
		
		$topicsQuery = array(
			'posts_per_page' =>4
		);
		
		$the_query = new WP_Query( $topicsQuery ); ?>
		<?php if ( $the_query->have_posts() ) : ?>
		  <!-- the loop -->
		  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		  	<article class="col_3 overlay" style="background-image:url('<?php $image_id = get_post_thumbnail_id();
		  		  $image_url = wp_get_attachment_image_src($image_id,'thumbnail', true);
		  		  echo $image_url[0]; ?> ');">
		  		<h3 class="overlayTitle"><?php the_category('<br>'); ?>
		  		</h3>
		
		  		<div class="description">
		  			<a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
		  		</div>

		  	</article>
	  <?php endwhile; ?>
	  <!-- end of the loop -->

	  <!-- pagination here -->

	  <?php wp_reset_postdata(); ?>

	<?php endif; ?>
		
	</section>
	
	<section class="row">
	
			<div class="col_3c video">
				<h3>Videos</h3>
				<?php 
				// the query
		
				$videoQuery = array(
					'post_type' =>'video'
				);
		
				$the_query = new WP_Query( $videoQuery ); ?>
				<?php if ( $the_query->have_posts() ) : ?>
				  <!-- the loop -->
				  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					  <?php the_content(); ?>
			  		 	 
		 
			  <?php endwhile; ?>
			  <!-- end of the loop -->

		 

			  <?php wp_reset_postdata(); ?>

			<?php endif; ?>
			</div>

			<h3>@SaludConSabila</h3>
			<div class="col_3">
				<div class="twitter">
				<a class="twitter-timeline" href="https://twitter.com/saludconsabila" data-widget-id="397604507627974656" data-theme="light" width="290" height="430">Tweets by @saludconsabila</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>
				
			</div>

	</section>	

	<section class="row fakefooter">
		<div class="col_3">
			<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php esc_attr_e( get_bloginfo('name'), 'Salud con Sábila' ); ?>" class="bottom logo" rel="home" style="background-image:url('<?php echo get_stylesheet_directory_uri(); ?>/images/salud-con-sabila.svg');">
				Salud con Sábila
		</a>
		</div>
		
		<div class="col_3c padding_15_0">
			
		<ul class="flatlist">
				<?php
			
				$categoryList = array (
				'orderby' => 'name',
				'title_li' => __('')
				);
		
				wp_list_categories( $categoryList ); 
			
				?>
		</ul>
		</div>
	</section>
<?php get_footer(); ?>