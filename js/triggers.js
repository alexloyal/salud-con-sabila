/*	CarouFredSel: a circular, responsive jQuery carousel.
	Configuration created by the "Configuration Robot"
	at caroufredsel.dev7studios.com
*/

$(document).ready(function(){
	$("#features").carouFredSel({
		width: "auto",
		height: "auto",
		responsive:true,
		items: {
			visible: 1,
			width: null,
			height: "100%"
		},
		scroll: {
			duration: 600,
			pauseOnHover: true,
			easing:"quadratic",
			fx:"scroll"
		},
		auto: 16000,
		prev: {
			button: "a.prev",
			key: "left"
		},
		next: {
			button: "a.next",
			key: "right"
		},
		pagination: {
			container:"#featuresNav ul.dots",
			anchorBuilder	: function( nr ) {
						return '<li><a href="#'+nr+'">•</a></li>';}
		}, 
		swipe: true
	});
});	