<?php
	
function codex_custom_init() {
  $labels = array(
    'name'               => 'Videos',
    'singular_name'      => 'Video',
    'add_new'            => 'Add New',
    'add_new_item'       => 'Add New Video',
    'edit_item'          => 'Edit Video',
    'new_item'           => 'New Video',
    'all_items'          => 'All Videos',
    'view_item'          => 'View Video',
    'search_items'       => 'Search Videos',
    'not_found'          => 'No Videos found',
    'not_found_in_trash' => 'No Videos found in Trash',
    'parent_item_colon'  => '',
    'menu_name'          => 'Videos'
  );

  $args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
	'taxonomies'		 => array('category','post_tag'),
    'rewrite'            => array( 'slug' => 'video' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
  );

  register_post_type( 'Video', $args );
}
add_action( 'init', 'codex_custom_init' );
	
?>