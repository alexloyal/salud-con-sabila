<?php get_header(); ?>
<!-- archive.php -->
<section id="archive" role="main">
<header class="header row">
	
<h1 class="entry-title">
	<?php 
if ( is_day() ) { printf( __( 'Daily Archives: %s', 'blankslate' ), get_the_time(get_option('date_format') ) ); }
elseif ( is_month() ) { printf( __( 'Monthly Archives: %s', 'blankslate' ), get_the_time('F Y') ); }
elseif ( is_year() ) { printf( __( 'Yearly Archives: %s', 'blankslate' ), get_the_time('Y') ); }
else { _e( 'Archives', 'blankslate' ); }
?></h1>
</header>



<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="grid">
	<article class="col_3 overlay" style="background-image:url('<?php $image_id = get_post_thumbnail_id();
		  $image_url = wp_get_attachment_image_src($image_id,'thumbnail', true);
		  echo $image_url[0]; ?> ');">
		<h3 class="overlayTitle"><?php the_category('<br>'); ?>
		</h3>
		
		<div class="description">
			<a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
		</div>

	</article>

	</div>
<?php endwhile; endif; ?>
<?php get_template_part('nav', 'below'); ?>

</section>

<?php get_footer(); ?>