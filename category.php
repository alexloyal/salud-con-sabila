<?php get_header(); ?>

<!-- category -->
<section id="category" role="main">
<header class="header row">
<h1 class="entry-title"><?php _e( '', 'blankslate' ); ?><?php single_cat_title(); ?></h1>
<?php if ( '' != category_description() ) echo apply_filters('archive_meta', '<div class="archive-meta">' . category_description() . '</div>'); ?>
</header>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="grid">
	<article class="col_3 overlay" style="background-image:url('<?php $image_id = get_post_thumbnail_id();
		  $image_url = wp_get_attachment_image_src($image_id,'thumbnail', true);
		  echo $image_url[0]; ?> ');">
		<h3 class="overlayTitle"><?php the_category('<br>'); ?>
		</h3>
		
		<div class="description">
			<a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
		</div>

	</article>

	</div>
<?php endwhile; endif; ?>
<?php get_template_part('nav', 'below'); ?>
</section>

<?php get_footer(); ?>