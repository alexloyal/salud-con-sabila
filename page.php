<?php get_header(); ?>
<article id="page" class="row" role="main">
	
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<section id="post-<?php the_ID(); ?>" <?php post_class('col_3c'); ?>>

		<header class="header">
		<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
		</header>

		<section class="entry-content">
		<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
		<?php the_content(); ?>
		<div class="entry-links"><?php wp_link_pages(); ?></div>
		</section>
	</section>



	<?php endwhile; endif; ?>


	<div class="col_3">
		<?php get_sidebar();?>
	</div>
	
</article>

<?php get_footer(); ?>