<aside id="sidebar" role="complementary">
	
<section id="aparte" class="widget-area">	
	<?php
	
	$aparte = get_post_custom_values('aparte',get_the_ID());
	
	if (! empty($aparte)) {

		echo $aparte[0];
	}
	
	?>
	
	
</section>

<section id="postMeta" class="widget-area">
	
	<span class="cat-links"><?php _e( 'Secciones: ', 'blankslate' ); ?><?php the_category(', '); ?></span>
	<span class="tag-links"><?php the_tags(); ?></span>
</section>
	
<?php if ( is_active_sidebar('primary-widget-area') ) : ?>
<div id="primary" class="widget-area border left top">
<ul class="xoxo">
<?php dynamic_sidebar('primary-widget-area'); ?>
</ul>
</div>
<?php endif; ?>
</aside>

<!-- adding a comment -->