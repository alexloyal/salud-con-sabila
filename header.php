<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title(' | ', true, 'right'); ?></title>
<!-- Mobile viewport optimized: h5bp.com/viewport -->


<!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/34gs-queries.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/34gs.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/flexslider.css" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.js"></script>
<?php wp_head(); ?>
<!-- header -->
</head>
<body <?php body_class('honey'); ?>>
	<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<div class="container wrapper">
	<header class="row">
		<div class="col_3">
			<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php esc_attr_e( get_bloginfo('name'), 'Salud con Sábila' ); ?>" class="top logo" rel="home" style="background-image:url('<?php echo get_stylesheet_directory_uri(); ?>/images/salud-con-sabila.svg');">
				Salud con Sábila
		</a>
		</div>
		<div class="col_3c">
			  <div class="navsearchsocial">

				<?php get_search_form(); ?>

    			
					
			 
				<div class="social">
    				<ul>
    					<li><a href="<?php echo of_get_option('facebook_url',''); ?>" class="facebook"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook.png" alt="Facebook"></a></li>
    					<li><a href="<?php echo of_get_option('twitter_url',''); ?>" class="twitter"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/twitter.png" alt="Twitter"></a></li>
    				</ul>
    			</div>
			   </div>
			<nav id="menu" class="nav" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
			</nav>
		</div>
  	</header>
